package codingBatExercices;

public class Warmup2 {
    public static void main(String[] args) {

    }

    //    Given a string and a non-negative int n, return a larger string that is n copies of the original string.
//
//            stringTimes("Hi", 2) → "HiHi"
//    stringTimes("Hi", 3) → "HiHiHi"
//    stringTimes("Hi", 1) → "Hi"
    public static String stringTimes(String str, int n) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < n; i++) {
            stringBuilder.append(str);
        }
        return stringBuilder.toString();
    }

    //    Given a string and a non-negative int n, we'll say that the front of the string is the first 3 chars,
    //    or whatever is there if the string is less than length 3. Return n copies of the front;
//
//    frontTimes("Chocolate", 2) → "ChoCho"
//    frontTimes("Chocolate", 3) → "ChoChoCho"
//    frontTimes("Abc", 3) → "AbcAbcAbc"
    public static String frontTimes(String str, int n) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < n; i++) {
            if (str.length() > 2) {
                s.append(str.substring(0, 3));
            } else s.append(str);
        }
        return s.toString();
    }

    //    Count the number of "xx" in the given string. We'll say that overlapping is allowed, so "xxx" contains 2 "xx".
//
//    countXX("abcxx") → 1
//    countXX("xxx") → 2
//    countXX("xxxx") → 3
    static int countXX(String str) {
        //will try recurence on this one
        return 0;
    }

    //class.end
}
