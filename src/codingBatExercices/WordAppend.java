package codingBatExercices;

import java.util.HashMap;
import java.util.Map;

public class WordAppend {

    public String wordAppendMethod(String[] array) {
        Map<String, Integer> map = new HashMap<>();
        String result = "";
        for (String element : array) {
            if (map.containsKey(element)) {
                int value = map.get(element)+1;
                if(value%2 ==0){
                    result += element;
                }
                map.put(element, value);
            } else {
                map.put(element, 1);
            }
        }
        return result;
    }

}
