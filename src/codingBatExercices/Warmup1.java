package codingBatExercices;

import java.util.Arrays;
import java.util.Locale;
import java.util.stream.Stream;

public class Warmup1 {

    public static void main(String[] args) {

        System.out.println(loneTeen(13, 99));
        System.out.println(loneTeen(21, 19));
        System.out.println(loneTeen(13, 13));
        System.out.println(delDel("adelbc"));
        System.out.println(intMax(1, 2, 3));
        System.out.println(intMax(1, 3, 2));
        System.out.println(intMax(3, 2, 1));
    }

    public static boolean loneTeen(int a, int b) {
        return ((isTeen(a) && !isTeen(b)) || (!isTeen(a) && isTeen(b)));
    }

    public static boolean isTeen(int a) {
        return (a >= 13 && a <= 19);
    }

    public static String delDel(String str) {
        StringBuilder s = new StringBuilder();
        s.append(str);

        if (s.length() > 3) {
            if (str.substring(1, 4).equals("del")) s.replace(1, 4, "");
            return s.toString();
        } else return str;

    }

    public static boolean mixStart(String str) {
        if (str.length() > 2) {
            return (str.substring(1, 3).equals("ix"));
        } else return false;
    }

    public static String startOz(String str) {
        StringBuilder s = new StringBuilder();
        if (str.equals("")) return "";
        if (str.length() > 1) {
            if (str.charAt(0) == 'o') s.append("o");
            if (str.charAt(1) == 'z') s.append('z');
        } else if (str.charAt(0) == 'o') s.append("o");
        return s.toString();
    }

    public static int intMax(int a, int b, int c) {
        if (a > b || a > c) {
            return a;
        }
        if (b > a || b > c) {
            return b;
        } else {
            return c;
        }
    }

    public static int close10(int a, int b) {
        if (a == b) return 0;
        if (Math.abs(a - 10) < Math.abs(b - 10)) return a;
        else return b;
    }

    public static boolean in3050(int a, int b) {
        return ((checkIntInRange(a, 30, 40) && checkIntInRange(b, 30, 40)) ||
                (checkIntInRange(a, 40, 50) && checkIntInRange(b, 40, 50)));
    }

    public static boolean checkIntInRange(int x, int min, int max) {
        return (x >= min && x <= max);
    }

    public static boolean aGreaterThanB(int a, int b) {
        return (a > b);
    }

    public static int max1020(int a, int b) {
        if (checkIntInRange(a, 10, 20)) {
            if (checkIntInRange(b, 10, 20)) {
                if (aGreaterThanB(b, a)) return b;
            }
            return a;
        } else if (checkIntInRange(b, 10, 20)) return b;

        return 0;
    }

    public static boolean stringE(String str) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'e') count++;
        }
        return (count >= 1 && count <= 3);
    }

    public boolean lastDigit(int a, int b) {
        return (a % 10 == b % 10);
    }

    public String endUp(String str) {
        if (str.length() > 2) {
            return str.substring(0, str.length() - 3) + str.substring(str.length() - 3, str.length()).toUpperCase(Locale.ROOT);
        } else return str.toUpperCase(Locale.ROOT);
    }

    public static String everyNth(String str, int n) {
        int index = 0;
        StringBuilder s = new StringBuilder();

        do{s.append(str.charAt(index));
            index+=n;
        }
            while (index <= str.length()-1);

            return s.toString();
    }





}
